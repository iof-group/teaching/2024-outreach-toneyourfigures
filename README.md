# Tone your figures for the summer
## A seminar on FAIR principles, project management and data visualization

## Content
./presentations:
- Research Data Management presentation
- FAIR principles and useful tools presentation

./2024-viz-dos-donts:
- Source code and article on data visualization

## Author
Imaging and Optics Facility [IOF@ist.ac.at](IOF@ist.ac.at) at Institute of Science and Technology Austria (ISTA)

## License and Usage
The content of this workshop is publicly available: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation in version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License v3 for more details. You should have received a copy of the GNU General Public License v3 along with this program. If not, see https://www.gnu.org/licenses/. 

Contact the Technology Transfer Office, ISTA, Am Campus 1, A-3400 Klosterneuburg, Austria, +43-(0)2243 9000, twist@ist.ac.at, for commercial licensing opportunities.
