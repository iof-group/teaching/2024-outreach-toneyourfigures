## Instructions
https://wiki-biop.epfl.ch/en/ipa/inkscape-figure

## Repository
https://gitlab.com/doctormo/inkscape-imagej-panel

## Examples
Seaborn
```
import seaborn as sns

df = sns.load_dataset("penguins")

sns.boxplot(data=df, x="island", y="body_mass_g", hue="sex")
sns.swarmplot(data=df, x="island", y="body_mass_g", hue="sex", dodge=True, color='gray', size=3)
```

Multipanel
```
from matplotlib import pyplot as plt
import seaborn as sns

df = sns.load_dataset("penguins")
f,  ax_dict = plt.subplot_mosaic(
	[
	["A", "B"],
	["C", "C"]		
])

sns.violinplot(data=df, x="species", y="body_mass_g", ax=ax_dict["A"])
sns.scatterplot(data=df, x="bill_length_mm", y="bill_depth_mm", ax=ax_dict["B"], hue="sex")

sns.histplot(data=df, x="body_mass_g", ax=ax_dict["C"], kde=True, hue="island")

ax_dict["A"].set(ylabel="Body Mass [gr]", xlabel="Species")
ax_dict["B"].set(ylabel="Bill Depth [mm]", xlabel="Bill Length [mm]")
ax_dict["C"].set( xlabel="Body Mass [g]")

for label, ax in ax_dict.items():
	ax.text(-0.1, 1.15, label, transform=ax.transAxes, fontweight='bold', fontsize=20, va='top', ha='right')
plt.tight_layout()
```

ImageJ
```
run("Fly Brain");
run("Z Project...", "projection=[Max Intensity]");
run("Scale Bar...", "width=200 height=200 location=[Upper Right] horizontal bold overlay");
```

R script
```
data(mtcars)
hist(mtcars$mpg)
```