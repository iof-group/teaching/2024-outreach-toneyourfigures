% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames,x11names}{xcolor}
%
\documentclass[
  letterpaper,
  DIV=11,
  numbers=noendperiod]{scrartcl}

\usepackage{amsmath,amssymb}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
\usepackage{lmodern}
\ifPDFTeX\else  
    % xetex/luatex font selection
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
% Make \paragraph and \subparagraph free-standing
\ifx\paragraph\undefined\else
  \let\oldparagraph\paragraph
  \renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
  \let\oldsubparagraph\subparagraph
  \renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi


\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}\usepackage{longtable,booktabs,array}
\usepackage{calc} % for calculating minipage widths
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\KOMAoption{captions}{tableheading}
\makeatletter
\@ifpackageloaded{caption}{}{\usepackage{caption}}
\AtBeginDocument{%
\ifdefined\contentsname
  \renewcommand*\contentsname{Table of contents}
\else
  \newcommand\contentsname{Table of contents}
\fi
\ifdefined\listfigurename
  \renewcommand*\listfigurename{List of Figures}
\else
  \newcommand\listfigurename{List of Figures}
\fi
\ifdefined\listtablename
  \renewcommand*\listtablename{List of Tables}
\else
  \newcommand\listtablename{List of Tables}
\fi
\ifdefined\figurename
  \renewcommand*\figurename{Figure}
\else
  \newcommand\figurename{Figure}
\fi
\ifdefined\tablename
  \renewcommand*\tablename{Table}
\else
  \newcommand\tablename{Table}
\fi
}
\@ifpackageloaded{float}{}{\usepackage{float}}
\floatstyle{ruled}
\@ifundefined{c@chapter}{\newfloat{codelisting}{h}{lop}}{\newfloat{codelisting}{h}{lop}[chapter]}
\floatname{codelisting}{Listing}
\newcommand*\listoflistings{\listof{codelisting}{List of Listings}}
\makeatother
\makeatletter
\makeatother
\makeatletter
\@ifpackageloaded{caption}{}{\usepackage{caption}}
\@ifpackageloaded{subcaption}{}{\usepackage{subcaption}}
\makeatother
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi
\usepackage[]{biblatex}
\addbibresource{references.bib}
\usepackage{bookmark}

\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\urlstyle{same} % disable monospaced font for URLs
\hypersetup{
  pdftitle={Dos and Don'ts of data visualization},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={Blue},
  pdfcreator={LaTeX via pandoc}}

\title{Dos and Don'ts of data visualization}
\usepackage{etoolbox}
\makeatletter
\providecommand{\subtitle}[1]{% add subtitle to \maketitle
  \apptocmd{\@title}{\par {\large #1 \par}}{}{}
}
\makeatother
\subtitle{A few sample cases to understand the importance of proper data
visualization}
\author{Marco Dalla Vecchia}
\date{2024-06-12}

\begin{document}
\maketitle

\renewcommand*\contentsname{Table of contents}
{
\hypersetup{linkcolor=}
\setcounter{tocdepth}{3}
\tableofcontents
}
\subsection{Introduction}\label{introduction}

So, you have run your big analysis and you are ready to create the
figures for your great paper that will surely win you the Nobel Prize?
Great!

\begin{quote}
\emph{How hard could it be, am I right??}
\end{quote}

Here are a few things to consider next time you make your figures for a
scientific publication. Remember, these are not strict rules but gentle
reminders and recommendations. As always, to every rule, there is an
exception. Just use your 🧠.

\subsection{Using appropriate graphs for the
message}\label{using-appropriate-graphs-for-the-message}

It might sound obvious, but you would be surprised to see how many cases
of simple data can be overcomplicated by the wrong graph, or how often a
message can be biased by choosing a type of plot that doesn't fit its
own data. Let's take a few examples.

\subsubsection{Pie-charts}\label{pie-charts}

There are many articles explaining why
\href{https://www.ataccama.com/blog/why-pie-charts-are-evil}{pie charts
are evil}, \textcite{schwabish_2021} does a particularly good job of
discussing both sides of when you could and must not use pie charts to
visualize your data. Pie charts have many issues, and
Figure~\ref{fig-pies} has pretty much all of them. Take a look at
Figure~\ref{fig-pies}; here we are representing the population of each
country in Europe as a fraction of the total population (i.e., the sum
of all the pie slices equals the total European population). It's pretty
clear how, in both sides of the figure, \textbf{there are way too many
slices, too many labels, and too many colors}. The slices are hard to
compare, and there are slices that are too different from each other.
The pie chart on the right side is a little easier to read because it
has been descendently sorted by population size but still has too many
data points.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-representation-accuracy-fig-pies-output-1.png}

}

\caption{\label{fig-pies}Pie charts for the visualization of european
countries population}

\end{figure}%

I know, it might sounds silly to display all that data in a pie chart,
but Figure~\ref{fig-pies} still shows the point of how simple data can
be incredibly overcomplicated with the wrong chart type. Luckily, pie
charts are not often used in scientific publications.

\subsubsection{Bar plots}\label{bar-plots}

Imagine displaying the same data in a bar plot like in
Figure~\ref{fig-bars} instead. Here we can appreciate the data much
better. There are still a lot of countries, but it's not as
overwhelming; visually, the contrast between bars makes the
interpretation much easier and there is no need for different colors.
There are always good and bad ways to create the same figure, for
example you can appreciate how in the right panel of
Figure~\ref{fig-bars} comparing data is much harder simply because the
data \textbf{is sorted alphabetically instead of numerically}.

But please remember that \textbf{bar plots are meant to display single
values}. As we will see in Figure~\ref{fig-mean-sep}, using bars for
showing grouped mean values, is a quick way to misleading data
visualization.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-representation-accuracy-fig-bars-output-1.png}

}

\caption{\label{fig-bars}Bar charts for the visualization of european
countries population}

\end{figure}%

\subsubsection{Heat map}\label{heat-map}

Heatmaps are quite useful, especially when you have two-dimensional data
to display and you have many categories for each. They are often used in
scientific publications, but they can be tricky to interpret as well.
Look at Figure~\ref{fig-heatmap}, for example: it seems to be pretty
straightforward at first, but how easily can you appreciate the
\textbf{numerical difference between countries' populations or between
the population of a country} throughout the years? Notice how having
\textbf{a common scale across all countries} makes subtle differences in
some countries less noticeable and how hard it is to have an exact
estimation of the population.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-representation-accuracy-fig-heatmap-output-1.png}

}

\caption{\label{fig-heatmap}Heatmap for the visualization of european
countries population in different years}

\end{figure}%

\subsubsection{Time series graph}\label{time-series-graph}

In contrast with pie charts, it's quite common to find time series
graphs in publications. However, displaying temporal data can still be
challenging depending on the type of data, time resolution and quantity
of data to display. Let's take a look at Figure~\ref{fig-barsyears},
here we are displaying population in a few countries over time like we
did in Figure~\ref{fig-heatmap}, but this time in a bar plot format. You
might appreciate how this arrangement favors more the \textbf{comparison
between countries in each year rather than the population evolution
across years}. Also, in my opinion, the number and colors of the bars
are cluttering and needlessly complicating the figure.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-representation-accuracy-fig-barsyears-output-1.png}

}

\caption{\label{fig-barsyears}Barplot for the visualization of european
countries population in different years}

\end{figure}%

Let's compare exactly the same data displayed in Figure~\ref{fig-lines}
in a line graph. In this case, the emphasis is more on the growth of the
population of each country, but comparing across countries is also not
too difficult. Notice how, in this case, \textbf{the use of color helps
distinguish the lines and how replacing a legend with the direct
labeling} of the data lines makes it much quicker to read the graph.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-representation-accuracy-fig-lines-output-2.png}

}

\caption{\label{fig-lines}Lineplot for the visualization of european
countries population in different years}

\end{figure}%

\subsection{Truncated axis (i.e.~start your axis at
zero)}\label{sec-truncated-axis}

A classic potential \emph{`mistake'} in data visualization is the
incorrect usage of scaling. For example in
Figure~\ref{fig-bar-truncated-axis}, due to the nature of how we read
bars in a bar plot, \textbf{we tend to compare bars amongst each other},
rather than comparing each bar with the Y-axis scale (or origin). For
this reason, you can appreciate how the difference between the grades of
our three (potentially randomly named) students look much different on
the left side of Figure~\ref{fig-bar-truncated-axis} than on the right
side. This is risky! Whenever you can tweak the message simply by
adjusting the Y-axis origin, it's a sign of bad data representation.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-truncated-axis-fig-bar-truncated-axis-output-1.png}

}

\caption{\label{fig-bar-truncated-axis}Comparing barplots with truncated
or complete Y-axis.}

\end{figure}%

There are exceptions however; notice how in
Figure~\ref{fig-line-truncated-axis}, having the truncated Y-axis on the
left side helps to describe the difference between grades of the
students across the year compared to the right side. Because we tend to
read line plots as evolution across time and not so much as difference
between the lines, we could argue that using a truncated axis in this
case is acceptable.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-truncated-axis-fig-line-truncated-axis-output-1.png}

}

\caption{\label{fig-line-truncated-axis}Comparing lineplots with
truncated or complete Y-axis.}

\end{figure}%

\subsection{Comparing values of different
scales}\label{comparing-values-of-different-scales}

\subsubsection{Broken axis}\label{broken-axis}

In Section~\ref{sec-truncated-axis}, we have seen that changing the
scaling by adjusting the origin of the axis of a figure can change the
message a figure is displaying. In Figure~\ref{fig-broken-ax}, we can
see population data across three different countries with vastly
different scales. The issue here is that China's population is several
factors higher compared to Italy's or Austria's population. In
Figure~\ref{fig-broken-ax}, I showed three different ways to deal with
data on such different scales. In the left panel, we can appreciate the
raw data as it comes; it's a fair representation but it really makes it
difficult to compare the countries with each other. A common solution is
to use a \textbf{broken axis}, as displayed in
Figure~\ref{fig-broken-ax}'s right panel. Here, the Y-axis scale is not
continuous but simply clipped in a way that data between 100 million and
1300 million people is removed from the graph. Although visually the
comparison between bars is now much easier, this bar plot can be very
misleading: at first sight, China's population appears to be a factor of
3 higher rather than a factor of \textasciitilde20 in reality! Sure, the
broken Y-axis is quite clear, but it's one step closer to
misinterpreting the message of the figure. A much better way to deal
with data of different scales is the middle panel in
Figure~\ref{fig-broken-ax}, where the simple usage of a logarithmic
scale on the Y-axis (log scale) makes the comparison between bars easier
and fairer.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-broken-axes-fig-broken-ax-output-1.png}

}

\caption{\label{fig-broken-ax}Comparing values of different scales}

\end{figure}%

\subsubsection{Double axis}\label{double-axis}

Depending on the type and amount of data to be displayed, we can be
tempted to use double axes in our figures. In
Figure~\ref{fig-double-axis}, we can see an example of how Austria's
population is really on a whole different scale than the population of
China. Although this is certainly the case, we could easily make it look
like they are almost identical by using two independent Y-axes with
different scales! Sure, it allows a closer comparison of the data
between countries, but it can also mislead the reader into thinking that
the data scales are very close to each other.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-double-axis-fig-double-axis-output-1.png}

}

\caption{\label{fig-double-axis}Visual effect of using double Y-axis on
data of different scales.}

\end{figure}%

I hope it's clear how you can tell different stories with your figures
simply by adjusting the corresponding scales of the two Y-axes. Notice
how I tried to make things clearer by coloring the Y-axis labels with
the same color as the data lines.

\subsection{Mean separation}\label{sec-mean-sep}

\textcite{li_2024} has collected an ever-growing list of bad
visualization cases, but one particular one that we see over and over in
scientific literature, is the the habit of comparing and visualizing
sample means with each other.

In Figure~\ref{fig-mean-sep}, I replicated \textcite{li_2024} 's
\href{https://github.com/cxli233/FriendsDontLetFriends/blob/main/Results/dont_bar_plot.png}{original
figure} in Python to show this issue. On the left panel, we are using
bars (which should be used only for single values) to represent the mean
of a response for two groups of experimental treatments. Although adding
the error bars makes the bar plot a little more accurate, it's a
representation to avoid, mostly because it does not show how the
underlying data of the two groups really behaves! \textbf{Always be
careful to simplifying a whole population down to its mean and standard
deviation}.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-mean-separation-fig-mean-sep-output-1.png}

}

\caption{\label{fig-mean-sep}Replica of mean separation figure by
\textcite{li_2024}}

\end{figure}%

In Figure~\ref{fig-mean-sep} middle panel, we can appreciate how the
usage of boxplots at least highlights slightly different median values,
different data spreads (look at the
\href{https://en.wikipedia.org/wiki/Interquartile_range}{interquantile
range (IQR)}) and shows that in the second group there are a few
outliers that behave differently to the rest of the group.

The right panel of Figure~\ref{fig-mean-sep}, finally reveals the real
difference between groups, which, despite having very similar means and
standard deviations, actually show to be dramatically different.

When comparing and displaying mean values by group, try to always show
the underlying data points and pay attention when boiling data down to
mean and standard deviation.

\subsection{SuperPlots}\label{superplots}

In this section we go one step further to
Section~\ref{sec-truncated-axis} and take a look at a replica I created
of Figure 1 of \textcite{lord_superplots_2020} below. As we can see from
Figure~\ref{fig-bad-figure}, \textcite{lord_superplots_2020} not only
highlights that using average bar plot to display your groupss data is
not correct but it criticizes how simple mean-comparing tests (such as
t-tests) are often misused by considering the sample size as large as
all the data points that were acquired.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-superplots-fig-bad-figure-output-1.png}

}

\caption{\label{fig-bad-figure}Replica of bad figure from
\textcite{lord_superplots_2020}}

\end{figure}%

Imagine comparing the average response of many cells (quantified a
migration speed difference) between two treatments. We have repeated
this experiment three times, which is (for some reason) classically
considered the standard value for biological replicates. A common
mistake is to treat each single cell as a separate independent
measurement (i.e.~\(N=100\)), leading to \textbf{wrong and incredibly
low p-values}!

What we should really be doing is to consider each independent
experiment as a separate measurement as thus fairly comparing the data
between treatments (\(N=3\)) like shown in the right panel in
Figure~\ref{fig-good-figure}.

\begin{figure}[H]

\centering{

\includegraphics{index_files/figure-latex/..-source-code-superplots-fig-good-figure-output-1.png}

}

\caption{\label{fig-good-figure}Replica of good figure from
\textcite{lord_superplots_2020}}

\end{figure}%

But we can do much better! \textcite{lord_superplots_2020} coined the
concept of \textbf{SuperPlot}, which clearly showcases every single data
point, grouped average and fair comparion of biological variability
across measurements. In Figure~\ref{fig-good-figure} middle panel we are
clearly showing the three independent measurements and on the right
panel we are even providing information on which cell was measured in
which experiment.

Here I only replicated the middle panel of Figure 1 of
\textcite{lord_superplots_2020} (beause that was the only available
dataset), but check the entire publication to understand the correct
usage of SuperPlots and how they allow for better clarity and evaluation
of variability and reproducibility of your data!

Do you want to create your own \textbf{SuperPlot}? Here are a few
resources:

\begin{itemize}
\tightlist
\item
  Check out the original publication \textcite{lord_superplots_2020}: it
  contains examples on how to do simple SuperPlots in Excel, GraphPad
  Prism, R and Python
\item
  Check out the source code for this article
  \href{https://git.ista.ac.at/iof-group/teaching/2024-outreach-toneyourfigures}{here}
\item
  Use this
  \href{https://huygens.science.uva.nl/SuperPlotsOfData/}{online tool}
  by \href{https://github.com/JoachimGoedhart/SuperPlotsOfData}{Joachim
  Goedhart}
\end{itemize}

If you are confused about classical statistical testing, the famous
\(N\), and p-values, I cannot recommend enough \textcite{royle_2019}, in
which he tackles everything from proper experimental design to
appropriate data management and analysis.

\subsection{Data visualization reading
recommendations}\label{data-visualization-reading-recommendations}

Check out what's available in the ISTA library! For example:

\begin{itemize}
\tightlist
\item
  \textcite{kirk_2016}
\item
  \textcite{knaflic_2015}
\item
  \textcite{schwabish_2021}
\item
  \textcite{royle_2019} → great book, strongly recommended book for all
  beginning PhD students (especially if will be doing Microscopy and
  Cell Biology)
\end{itemize}

I already cited \textcite{li_2024} for Figure~\ref{fig-mean-sep}, but
check out the whole series \href{sec-truncated-axis}{on GitHub}.

\subsection{Source code}\label{source-code}

Do you want to replicate the figures I show in this article? Do you want
to reuse this material? Head over to the
\href{https://git.ista.ac.at/iof-group/teaching/2024-outreach-toneyourfigures}{IOF
GitLab repository}, but make sure to cite it correctly following the
license guideline.

\subsection{Python plotting libraries
comparison}\label{python-plotting-libraries-comparison}

Check out this other article for an extensive comparison of Pyton
plotting libraries
\href{https://mvgroup.gitlab.io/mvlearn/content-plotting-comparison/}{here}.


\printbibliography


\end{document}
