# Dos and Don'ts of data visualization
## A few sample cases to understand the importance of proper data visualization.

Read it [online here](https://pub.ista.ac.at/~mdallave/read/viz-dos-donts/main) or checkout the source code in this repository!

## Project structure
```
this repo
├── data (see data source below)
├── figures  (figures created with source-code)
├── source-code (all jupyter notebooks)
└── viz-dos-donts (containing HTML article source code)
```

## Author
Marco Dalla Vecchia, Imaging and Optics Facility [IOF@ist.ac.at](IOF@ist.ac.at) at Institute of Science and Technology Austria (ISTA)

## License and Usage
The content of this workshop is publicly available: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation in version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License v3 for more details. You should have received a copy of the GNU General Public License v3 along with this program. If not, see https://www.gnu.org/licenses/. 

Contact the Technology Transfer Office, ISTA, Am Campus 1, A-3400 Klosterneuburg, Austria, +43-(0)2243 9000, twist@ist.ac.at, for commercial licensing opportunities.

### Data source
- SuperPlots data in jcb_202001064_datas1.txt [original publication](https://doi.org/10.1083/jcb.202001064)
- world population data in API_SP.POP.TOTL_DS2_en_csv_v2_320414: [world data bank](https://data.worldbank.org/indicator/SP.POP.TOTL)
- list of european countires in europe-countries.txt: [efort.org](https://www.efort.org/wp-content/uploads/2017/11/List_of_countries_in_Europe.pdf)